namespace Uebung_05
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdZeichen = new System.Windows.Forms.Button();
            this.cmdArray = new System.Windows.Forms.Button();
            this.cmdEnde = new System.Windows.Forms.Button();
            this.txtAusgabe = new System.Windows.Forms.TextBox();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.cmd_streamwriter = new System.Windows.Forms.Button();
            this.cmd_reverse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmdZeichen
            // 
            this.cmdZeichen.Location = new System.Drawing.Point(431, 12);
            this.cmdZeichen.Name = "cmdZeichen";
            this.cmdZeichen.Size = new System.Drawing.Size(91, 23);
            this.cmdZeichen.TabIndex = 0;
            this.cmdZeichen.Text = "Zeichen";
            this.cmdZeichen.UseVisualStyleBackColor = true;
            this.cmdZeichen.Click += new System.EventHandler(this.cmdZeichen_Click);
            // 
            // cmdArray
            // 
            this.cmdArray.Location = new System.Drawing.Point(431, 41);
            this.cmdArray.Name = "cmdArray";
            this.cmdArray.Size = new System.Drawing.Size(91, 23);
            this.cmdArray.TabIndex = 1;
            this.cmdArray.Text = "Array";
            this.cmdArray.UseVisualStyleBackColor = true;
            this.cmdArray.Click += new System.EventHandler(this.cmdArray_Click);
            // 
            // cmdEnde
            // 
            this.cmdEnde.Location = new System.Drawing.Point(431, 178);
            this.cmdEnde.Name = "cmdEnde";
            this.cmdEnde.Size = new System.Drawing.Size(91, 23);
            this.cmdEnde.TabIndex = 2;
            this.cmdEnde.Text = "Ende";
            this.cmdEnde.UseVisualStyleBackColor = true;
            this.cmdEnde.Click += new System.EventHandler(this.cmdEnde_Click);
            // 
            // txtAusgabe
            // 
            this.txtAusgabe.Location = new System.Drawing.Point(12, 34);
            this.txtAusgabe.Multiline = true;
            this.txtAusgabe.Name = "txtAusgabe";
            this.txtAusgabe.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtAusgabe.Size = new System.Drawing.Size(351, 122);
            this.txtAusgabe.TabIndex = 3;
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(12, 194);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtStatus.Size = new System.Drawing.Size(351, 122);
            this.txtStatus.TabIndex = 4;
            // 
            // cmd_streamwriter
            // 
            this.cmd_streamwriter.Location = new System.Drawing.Point(431, 85);
            this.cmd_streamwriter.Name = "cmd_streamwriter";
            this.cmd_streamwriter.Size = new System.Drawing.Size(91, 24);
            this.cmd_streamwriter.TabIndex = 5;
            this.cmd_streamwriter.Text = "StreamWriter";
            this.cmd_streamwriter.UseVisualStyleBackColor = true;
            this.cmd_streamwriter.Click += new System.EventHandler(this.cmd_streamwriter_Click);
            // 
            // cmd_reverse
            // 
            this.cmd_reverse.Location = new System.Drawing.Point(431, 133);
            this.cmd_reverse.Name = "cmd_reverse";
            this.cmd_reverse.Size = new System.Drawing.Size(91, 23);
            this.cmd_reverse.TabIndex = 6;
            this.cmd_reverse.Text = "Reverse";
            this.cmd_reverse.UseVisualStyleBackColor = true;
            this.cmd_reverse.Click += new System.EventHandler(this.cmd_reverse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Ausgabe:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Status:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 373);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmd_reverse);
            this.Controls.Add(this.cmd_streamwriter);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.txtAusgabe);
            this.Controls.Add(this.cmdEnde);
            this.Controls.Add(this.cmdArray);
            this.Controls.Add(this.cmdZeichen);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdZeichen;
        private System.Windows.Forms.Button cmdArray;
        private System.Windows.Forms.Button cmdEnde;
        private System.Windows.Forms.TextBox txtAusgabe;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Button cmd_streamwriter;
        private System.Windows.Forms.Button cmd_reverse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}