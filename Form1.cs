using System;
// using System.Collections.Generic;
// using System.ComponentModel;
// using System.Data;
// using System.Drawing;
using System.IO;  // Assembly für Massenspeicher
// using System.Linq;
// using System.Text;
// using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uebung_05
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cmdEnde_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmdZeichen_Click(object sender, EventArgs e)
        {
            // Array deklarieren
            byte[] zeichen = new byte[26] { 65, 66, 67, 68, 69, 70, 71, 72,
                                            73, 74, 75, 76, 77, 78, 79, 80,
                                            81, 82, 83, 84, 85, 86, 87, 88, 89, 90 };

            // FileStream deklarieren
            FileStream fs = new FileStream("C:\\Temp\\Test.txt", FileMode.Create);

            // Array in Datei schreiben
            for (int i = 0; i < 26; i++)
            {
                fs.WriteByte(zeichen[i]);
                txtAusgabe.AppendText(Convert.ToString((char)zeichen[i]));
            }

            // Zeilenvorschub in Textfeld
            txtAusgabe.AppendText("\r\n");

            // Datei schließen
            fs.Close();

            // Meldung der Fertigstellung ausgeben
            txtStatus.Text = "Zeichenweise Ausgabe ist fertig!";

        }

        private void cmdArray_Click(object sender, EventArgs e)
        {
            // Array deklarieren
            byte[] zeichen = new byte[26] { 97, 98, 99, 100, 101, 102, 103, 104, 105,
                                            106, 107, 108, 109, 110, 111, 112, 113, 114,
                                            115, 116, 117, 118, 119, 120, 121, 122 };

            // Filestream
            FileStream fs = new FileStream("C:\\Temp\\Test.txt", FileMode.Append);

            // Zeilenvorschub in Datei schreiben
            fs.WriteByte(13);
            fs.WriteByte(10);

            // Array in Datei schreiben
            fs.Write(zeichen, 0, 26);

            // Ein Zeichen zurückgehen
            fs.WriteByte(8);
            fs.Position = fs.Position - 3;

            // Array in Textfeld ausgeben
            foreach (byte z in zeichen)
                txtAusgabe.AppendText(Convert.ToString((char)z));
            txtAusgabe.AppendText("\r\n");

            // Datei schließen
            fs.Close();

            // Meldung der Ferigstellung ausgeben
            txtStatus.Text = "Ausgabe des Arrays ist fertig!";

        }

        private void cmd_streamwriter_Click(object sender, EventArgs e)
        {
            // Array deklarieren
            byte[] zeichen = new byte[26] { 65, 66, 67, 68, 69, 70, 71, 72, 73,
                                            74, 75, 76, 77, 78, 79, 80, 81, 82,
                                            83, 84, 85, 86, 87, 88, 89, 90 };

            // Filestream deklarieren
            FileStream fs = new FileStream(@"C:\Temp\Text_2.txt", FileMode.Create, FileAccess.Write);

            // StreamWriter deklarieren
            StreamWriter sw = new StreamWriter(fs);

            // Array in die Datei C:\Temp\Text_2.txt schreiben
            for (int i = 0; i < 26; i++)
            {
                sw.WriteLine((char) zeichen[i]);
                txtAusgabe.AppendText(Convert.ToString((char)zeichen[i]));
                txtAusgabe.AppendText("\r\n");
            }

            // Ausgabe in die Statusbox
            txtAusgabe.AppendText("\r\n");
            txtStatus.Text = "Zeichenweise Ausgabe ist fertig!";

            // Filestream und StreamWriter müssen wieder geschlossen werden
            sw.Close();
            fs.Close();

;
        }

        private void cmd_reverse_Click(object sender, EventArgs e)
        {
            // Array deklarieren

            byte[] zeichen = new byte[26] { 65, 66, 67, 68, 69, 70, 71, 72, 73,
                                            74, 75, 76, 77, 78, 79, 80, 81, 82,
                                            83, 84, 85, 86, 87, 88, 89, 90 };

            // Filestream deklarieren
            FileStream fs = new FileStream(@"C:\Temp\Text_2.txt", FileMode.Append, FileAccess.Write);

            // StreamWriter deklarieren
            StreamWriter sw = new StreamWriter(fs);

            for (int i = 0; i < 26; i = i + 1)
            {
                sw.WriteLine((char) zeichen[25 - i]);
                txtAusgabe.AppendText(Convert.ToString((char)zeichen[25 - i]));
                txtAusgabe.AppendText("\r\n");
            }

            // Ausgabe in die Statusbox
            txtAusgabe.AppendText("\r\n");
            txtStatus.Text = "Zeichenweise Ausgabe ist fertig!";

            // Filestream und StreamWriter müssen wieder geschlossen werden
            sw.Close();
            fs.Close();
        }
    }
}